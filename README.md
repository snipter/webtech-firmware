# WebTechDay #2: Firmware for Demo

This is firmware script to run on your RaspberryPI.

## Install

Required:

- RaspberryPI
- Node: 6.7.0
- NPM: 3.10.3
- [BCM2835 driver](http://www.airspayce.com/mikem/bcm2835/) for GPIO
- Google Service credentials in `serviceAccount.json` file (check [this doc](https://firebase.google.com/docs/server/setup#add_firebase_to_your_app) to findout how to make it).

Install:

```
npm install
```

Run:

```
node index.js
```

## Contacts

Jaroslav Khorishchenko (Ukraine)

**Email**: [websnipter@gmail.com](mailto:websnipter@gmail.com)

**Facebook**: [http://fb.com/snipter](http://fb.com/snipter)