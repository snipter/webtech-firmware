var firebase = require("firebase");
var log = require('sm-log');
var sensor = require('node-dht-sensor');

var m = 'iot';

var DHT11 = 11;
var DHT22 = 22;

var sensorType = DHT11;
var sensorPin = 4;
var sensorId = "-KTJv7CzN0su5rZhdSfu";

firebase.initializeApp({
	serviceAccount: "./serviceAccount.json",
	databaseURL: "https://web-tech-day.firebaseio.com"
});

// get data
var db = firebase.database();

var sensorCurrentPath = "sensors/" + sensorId + "/current";
log.debug('sensor current path: ' + sensorCurrentPath, m);
var sensorCurrentRef = db.ref(sensorCurrentPath);

var sensorHistoryPath = 'history/' + sensorId;
log.debug('sensor history path: ' + sensorHistoryPath, m);
var sensorHistoryRef = db.ref(sensorHistoryPath);


// updating current sensor data
setInterval(function(){
	getSensorData(function(err, data){
		if(err) return;
		setCurrentSensorData(data);
	});
}, 1000);

// adding sensor data to history
setInterval(function(){
	getSensorData(function(err, data){
		if(err) return;
		addSensorDataToHistory(data);
	});
}, 5000);

function setCurrentSensorData(data){
	log.debug('setting current sensor data', m);
	log.trace(data, m);
	sensorCurrentRef.set(data);
}

function addSensorDataToHistory(data){
	log.debug('adding current sensor data to history', m);
	log.trace(data, m);
	sensorHistoryRef.push(data);
}

/*============ Sensor ============*/

function getSensorData(cb){
	sensor.read(sensorType, sensorPin, function(err, tempr, hum) {
		if(err) return log.err(err, m, cb);
		var data = {
			tempr: tempr,
			hum: hum,
			ts: {".sv": "timestamp"}
		}
	    return cb(null, data);
	});
}